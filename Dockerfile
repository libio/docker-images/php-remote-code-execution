# To build:
# $ docker build -t php-exploit .
#
# To Run:
# $ docker run -tip 8080:8080 -e AUTH_USERNAME=root -e AUTH_PASSWORD=password php-exploit:latest
#
# To use:
# Visit http://localhost:8080 and enter commands

FROM php:7.3-apache

ENV APACHE_DOCUMENT_ROOT /entry
WORKDIR ${APACHE_DOCUMENT_ROOT}
EXPOSE 8080

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf \
    && sed -ri -e 's!Listen 80!Listen 8080!g' /etc/apache2/ports.conf \
    && sed -ri -e 's!<VirtualHost \*:80>!<VirtualHost *:8080>!g' /etc/apache2/sites-enabled/*.conf \
    && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

COPY web ${APACHE_DOCUMENT_ROOT}

USER 33
# CMD ["/bin/bash", "-uc", "htpasswd -b ${APACHE_DOCUMENT_ROOT}/.htpasswd ${AUTH_USERNAME} ${AUTH_PASSWORD} && exec apache2-foreground"]
CMD ["apache2-foreground"]
